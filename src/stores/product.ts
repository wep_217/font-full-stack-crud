import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dailog = ref(false);
  const products = ref(<Product[]>[]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dailog, (newDialog, olDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      // console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        await productService.addNewProduct(editedProduct.value);
      }

      dailog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dailog.value = true;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ product ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    products,
    getProducts,
    dailog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
  };
});
