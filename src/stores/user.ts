import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dailog = ref(false);
  const users = ref(<User[]>[]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });

  watch(dailog, (newDialog, olDialog) => {
    if (!newDialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      // console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง user ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        await userService.updateUser(editedUser.value.id, editedUser.value);
      } else {
        await userService.addNewUser(editedUser.value);
      }

      dailog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก user ได้");
    }
    loadingStore.isLoading = false;
  }

  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dailog.value = true;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ user ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    users,
    getUsers,
    editedUser,
    dailog,
    saveUser,
    editUser,
    deleteUser,
  };
});
